import data.Person
import kotlinx.coroutines.runBlocking
import utils.loadAllResourceData
import utils.serializeResources
import utils.serializeResourcesByCountry
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.io.path.exists
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    // First arg is source, second arg is target.
    // If no args are supplied, program will use default arguments below.
    var sourceDirectory = "src/main/resources"
    var outputDirectory = "src/main/output"

    if (args.isNotEmpty()) {
        sourceDirectory = args[0]
    }
    if (args.size > 1) {
        outputDirectory = args[1]
    }

    val outputPath = Paths.get(outputDirectory)
    if (!outputPath.exists()) {
        Files.createDirectory(outputPath)
    }

    var mainData: List<Person>

    runBlocking {
        val loadTime = measureTimeMillis {
            mainData = loadAllResourceData(sourceDirectory)
        }

        println("File reading of all files completed within $loadTime ms")

        serializeResources(outputDirectory, mainData)

        val groupTime = measureTimeMillis {
            serializeResourcesByCountry(outputDirectory, mainData.groupBy { it.country })
        }

        println("Finished serialization after grouping by country within $groupTime ms")
    }
}
