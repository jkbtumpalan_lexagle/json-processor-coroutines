package data

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

data class Person(
    val id: Int,
    val first_name: String,
    val last_name: String,
    val email: String,
    val gender: String,
    val ip_address: String,
    val favorite_animal: String,
    val favorite_car_brand: String,
    val favorite_color: String,
    val favorite_drug: String,
    val race: String,
    val country: String
)

val mapper = jacksonObjectMapper()
val writer: ObjectWriter = mapper.writer(DefaultPrettyPrinter())
