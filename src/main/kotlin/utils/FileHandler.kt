package utils

import com.fasterxml.jackson.module.kotlin.readValue
import data.Person
import data.mapper
import data.writer
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.io.File

fun readFileUsingBufferedReader(fileName: String): List<Person> =
    mapper.readValue(File(fileName).bufferedReader().readText())

fun writeFilesUsingObjectWriter(directory: String, country: String?, jsonArray: List<Person>) {
    val fileName = country ?: "00_COMBINED_MOCK_DATA"
    writer.writeValue(File("$directory/$fileName.json"), jsonArray)
}

suspend fun loadAllResourceData(directory: String): List<Person> {
    val resourceDirectory = File(directory)
    val resourceList = resourceDirectory.listFiles() as Array<File>

    val mockInputData = mutableListOf<Person>()

    coroutineScope {
        for (file_ in resourceList) {
            launch {
                if (file_.isFile && !file_.isHidden && file_.extension == "json") {
                    mockInputData.addAll(
                        readFileUsingBufferedReader("$directory/${file_.name}")
                    )
                }
            }
        }
    }

    return mockInputData
}

fun serializeResources(directory: String, data: List<Person>) {
    writeFilesUsingObjectWriter(directory, null, data)
}
suspend fun serializeResourcesByCountry(directory: String, fileContent: Map<String, List<Person>>) {
    coroutineScope {
        for (country in fileContent) {
            launch {
                writeFilesUsingObjectWriter(directory, country.key, country.value)
            }
        }
    }
}
